#!/bin/sh
VPN=tun0
LAN=eth0
LOCAL=10.0.0.0

block_ports () {
/usr/builtin/sbin/iptables -F
cat /opt/etc/portblock.conf | \
while read PORTS; do
    /usr/builtin/sbin/iptables -A INPUT -i $VPN -p tcp --destination-port $PORTS -j DROP
    /usr/builtin/sbin/iptables -A INPUT -i $VPN -p udp --destination-port $PORTS -j DROP
done
}

transmission_ports () {
#do this better with tpc and udp sorting
/opt/bin/lsof -p $(pidof transmission-daemon) | /opt/bin/grep UDP | /opt/bin/cut -d ':' -f 2 | \
while read PORTS; do
    /usr/builtin/sbin/iptables -A INPUT -i $VPN -m state --state RELATED,ESTABLISHED -p udp --dport $PORTS -j ACCEPT
    /usr/builtin/sbin/iptables -A OUTPUT -o $VPN -p udp --sport $PORTS -j ACCEPT
done
}

traffic_rules () {
iptables -A INPUT -i $VPN -p tcp -m conntrack --ctstate NEW -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,PSH,URG -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
iptables -A INPUT -i $VPN -f -j DROP
iptables -A FORWARD -m state --state INVALID -j DROP
iptables -A INPUT -i $LAN -s 127.0.0.0/8 -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags ALL NONE -j DROP
iptables -A INPUT -i $VPN -p tcp -m tcp --tcp-flags RST RST -m limit --limit 2/second --limit-burst 2 -j ACCEPT
iptables -A INPUT -i $VPN -p icmp -m icmp --icmp-type address-mask-request -j DROP
iptables -A INPUT -i $VPN -p icmp -m icmp --icmp-type timestamp-request -j DROP
iptables -A INPUT -i $VPN -p icmp -m limit --limit 2/second --limit-burst 2 -j ACCEPT
iptables -A INPUT -i $VPN -p icmp -j DROP
iptables -A INPUT -i $VPN -p tcp -m state --state NEW -m limit --limit 2/second --limit-burst 2 -j ACCEPT
iptables -A INPUT -i $VPN -p tcp -m state --state NEW -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
iptables -A INPUT -i $VPN -p tcp --tcp-flags ALL ALL -j DROP
iptables -A INPUT -i $VPN -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i $VPN -m conntrack --ctstate INVALID -j DROP
iptables -A OUTPUT -o $VPN -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o $VPN -m conntrack --ctstate INVALID -j DROP
iptables -I INPUT -i $VPN -p icmp --icmp-type 8 -j DROP
iptables -A INPUT -i $VPN -p icmp --icmp-type echo-reply -j DROP
iptables -A INPUT -i $VPN -p icmp -m icmp --icmp-type address-mask-request -j DROP
iptables -A INPUT -i $VPN -p icmp -m icmp --icmp-type timestamp-request -j DROP
iptables -A INPUT -i $LAN -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i $LAN -m conntrack --ctstate INVALID -j DROP
iptables -A OUTPUT -o $LAN -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o $LAN -m conntrack --ctstate INVALID -j DROP
iptables -A INPUT -i lo -p all -j ACCEPT
iptables -A OUTPUT -o lo -p all -j ACCEPT
}

logging_rules () {
iptables -N LOGGING
iptables -A INPUT -i $VPN -j LOGGING
iptables -A LOGGING -m limit --limit 2/min -j LOG --log-prefix "firewall: " --log-level 4
}

first_time_setup () {
cat << EOF | sudo tee /etc/rsyslog.d/firewall.conf
:msg,contains,"[firewall:" /opt/var/log/firewall.log
& stop
EOF

cat << EOF | sudo tee /etc/logrotate.d/firewall.conf
/opt/var/log/firewall.log
{
  rotate 7
  daily
  missingok
  notifempty
  delaycompress
  compress
  postrotate
  invoke-rc.d rsyslog rotate > /dev/null
  endscript
}
EOF
}

if [ ! -f /etc/logrotate.d/firewall.conf ]
then first_time_setup; fi

block_ports
transmission_ports
traffic_rules
logging_rules

iptables-save > /opt/etc/iptables.rules
echo "$(/usr/builtin/sbin/iptables -L | /opt/bin/grep -oE "(^ACCEPT|^DROP|^REJECT)" |/opt/bin/wc -l) rules loaded. Use iptables -L -v -n for stats"